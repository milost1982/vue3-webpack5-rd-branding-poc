
class MyFirstWebpackPlugin {
  apply (compiler) {
    compiler.hooks.done.tapAsync('MyFirstWebpackPlugin', (stats, callback) => {
      const assetNames = []
      for (const assetName in stats.compilation.assets) {
        assetNames.push(assetName)
      }
      console.log(assetNames.join('\n'))
      callback()
    })
  }
}

module.exports = MyFirstWebpackPlugin
