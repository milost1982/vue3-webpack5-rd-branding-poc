// const { getBrandedPath, createResolver } = require('./resolve')

function getWalletDir (input, importPrefix, walletsDir, wallet) {

}

function createResolver ({ importPrefix, walletsDir, wallet }) {
  return input => getWalletDir(input, importPrefix, walletsDir, wallet)
}

// const defaultOptions = {
//   importPrefix: '#brand#',
//   brandsDir: 'brands',
//   brand: ''
// }
const defaultOptions = {}

// let resolveBrandedComponentPath = null

function WalletResolverPlugin (options) {
  this.source = 'resolve'
  this.target = 'resolve'

  options = options || {}

  // defaultOptions.brand = process.env.VUE_APP_BRAND // at this point we should have this variable
  this.options = { ...defaultOptions, ...options }
}

WalletResolverPlugin.prototype.apply = function (resolver) {
  const target = resolver.ensureHook(this.target)
  const resolveBrandedComponentPath = createResolver(this.options)

  resolver.getHook(this.source).tapAsync('WalletResolverPlugin', function(request, resolveContext, callback) {
    // "this" here is not same as "this" in apply function
    const brandedPath = resolveBrandedComponentPath(request.request)
    if (brandedPath) {
      console.log('\nresolved brand component:', brandedPath)
      // console.log('this', this)
      const obj = Object.assign({}, request, {
        request: brandedPath
      })
      return resolver.doResolve(target, obj, null, resolveContext, callback)
    }
    callback()
  })
}

// function MyConventionResolver (source, target) {
//   this.source = source || 'resolve'
//   this.target = target || 'resolve'
// }

// MyConventionResolver.prototype.apply = function (resolver) {
//   const target = resolver.ensureHook(this.target)
//   resolver.getHook(this.source).tapAsync('MyConventionResolver', function(request, resolveContext, callback) {
//     const result = getBrandedPath(request.request)
//     if (result.isResolved) {
//       console.log('resolved brand component:', result.resolvedPath)
//       const obj = Object.assign({}, request, {
//         request: result.resolvedPath
//       })
//       return resolver.doResolve(target, obj, null, resolveContext, callback)
//     }

//     // if (request.request[0] === '#') {
//     //   const req = request.request.substr(1)
//     //   const obj = Object.assign({}, request, {
//     //     request: req //+ '/' + path.basename(req) + '.js'
//     //   })
//     //   console.log('MyConventionResolver obj:', obj)
//     //   // path.dirname(req)
//     //   return resolver.doResolve(target, obj, null, resolveContext, callback);
//     // }
//     callback()
//   })
// }

module.exports = WalletResolverPlugin
