/* eslint-disable */
import getBrandedPath from '../resolve'
process.env.VUE_APP_BRAND = 'BrandX'

describe('resolve', () => {
  test('relative paths', () => {
    expect(getBrandedPath('#brand#@/components/Login.vue').resolvedPath)
                    .toBe('@/components/brands/Login.undefined.vue')

    // getBrandedPath('#brand#./Login.vue')
    // getBrandedPath('#brand#@/components/Login')
    // getBrandedPath('#brand#./Login')
  })
})
