/* eslint-disable */
function getComponentPathInfo(componentPath) {
  // let slashChar = ''
  let lastSlash = componentPath.lastIndexOf('/')
  if (lastSlash === -1) {
    lastSlash = componentPath.lastIndexOf('\\')
    if (lastSlash === -1) {
      throw new Error('Unable to find slash in: ' + componentPath)
    }
  }
  const directory = componentPath.substring(0, lastSlash + 1)
  const filename = componentPath.substring(lastSlash + 1, componentPath.length)
  return { filename, directory }
}

function getBrandedComponentPath(componentPath) {
  const { filename, directory } = getComponentPathInfo(componentPath)
  // debugger

  // let extIndex = filename.lastIndexOf('.vue')
  // if (extIndex === -1) {
  //   return `${componentPath}.${process.env.VUE_APP_BRAND}`
  // }
  // debugger
  const output = directory + 'brands/' + filename + '.' + process.env.VUE_APP_BRAND + '.vue'

  return output
  // return `${componentPath.substring(0, extIndex)}.${process.env.VUE_APP_BRAND}${componentPath.substring(extIndex)}`
}

async function importByBrand(componentPath) {
  if (typeof componentPath !== 'string') {
    throw Error('componentPath must be string')
  }
  if (componentPath.length === 0) {
    throw new Error('componentPath must be non-empty')
  }
  const brandedComponentPath = getBrandedComponentPath(componentPath) //`${componentPath}.${process.env.VUE_APP_BRAND}`
  // console.log('importByBrand brandedComponentPath:', brandedComponentPath)

  const { default: component } = await import(/* webpackMode: 'eager' */ brandedComponentPath) // eslint-disable-line
  return component
}

module.exports = {importByBrandEx: importByBrand}
