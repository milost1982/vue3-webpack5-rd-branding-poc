const path = require('path');

const importPrefix = '#brand#'
const brandsDir = 'brands'
const brand = process.env.VUE_APP_BRAND
console.log('Brand resolver process.env.VUE_APP_BRAND:', process.env.VUE_APP_BRAND)
// const default_importPrefix = '#brand#'

function getBrandedPath (input) {
  const isInputValid = input && typeof input === 'string' && input.startsWith(importPrefix)
  if (!isInputValid) {
    return {
      isResolved: false,
      resolvedPath: input
    }
  }

  input = input.substring(importPrefix.length).split('\\').join('/') // convert all \ to /

  const dirname = path.dirname(input)
  const extension = path.extname(input) // eg: .vue
  const filename = path.basename(input, extension) // returns filename without extension (if any)
  // console.log(input)
  // console.log(dirname)
  // console.log(filename)
  return {
    isResolved: true,
    resolvedPath: `${dirname}/${brandsDir}/${filename}.${brand}${extension}`
  }
}

// console.log(getBrandedPath('#brand#@/components/Login.vue'))
// console.log(getBrandedPath('#brand#./Login.vue'))
// console.log(getBrandedPath('#brand#@/components/Login'))
// console.log(getBrandedPath('#brand#./Login'))

// const createResolver = ({ importPrefix = default_importPrefix, brandsDir, brand }) => {
// }

function getBrandedPath2 (input, importPrefix, brandsDir, brand) {
  if (!brand) { return null }

  const isInputValid = input && typeof input === 'string' && input.startsWith(importPrefix)
  if (!isInputValid) { return null } // invalid input

  input = input.substring(importPrefix.length).split('\\').join('/') // convert all \ to /

  const dirname = path.dirname(input)
  const extension = path.extname(input) // eg: .vue
  const filename = path.basename(input, extension) // returns filename without extension (if any)
  // console.log(input)
  // console.log(dirname)
  // console.log(filename)
  // let resolvedPath = dirname

  // // if no brandsDir specified look for branded components in the same directory
  // if (brandsDir) {
  //   resolvedPath += '/' + brandsDir
  // }
  // path.join(dirname, brandsDir)
  const resolvedPath = `${dirname}/${brandsDir}/${filename}.${brand}${extension}`
  return resolvedPath
}
function createResolver ({ importPrefix, brandsDir, brand }) {
  return input => getBrandedPath2(input, importPrefix, brandsDir, brand)
}

module.exports = {
  createResolver,
  getBrandedPath
}
