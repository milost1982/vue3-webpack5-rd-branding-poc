module.exports = {
  walletsRootDir: '',
  supportedWallets: [
    {
      name: 'iw',
      dirName: 'IW'
    },
    {
      name: 'gan',
      dirName: 'Gan'
    },
    {
      name: 'command',
      dirName: 'Command'
    },
    {
      name: 'fd',
      dirName: 'FD'
    }
  ]
}
