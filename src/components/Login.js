export default {
  data () {
    return {
      title: 'Login base',
      isLoggedIn: null
    }
  },
  methods: {
    login () {
      this.isLoggedIn = true
    },
    logout () {
      this.isLoggedIn = false
    }
  },
  computed: {
    btnText (newValue) {
      return newValue ? 'Logout' : 'Login'
    }
  }
}
