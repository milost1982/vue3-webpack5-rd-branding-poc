import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

const app = createApp(App).use(store).use(router)

// before mounting app let's initialize wallet

app.mount('#app')

// app.config.globalProperties.importByBrand2 = 'vue global property' // https://v3.vuejs.org/api/application-config.html#globalproperties
