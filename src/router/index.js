import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Wallet from '../views/Wallet.vue'
// import wallet from '#wallet#'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/wallet',
    name: 'Wallet',
    component: Wallet
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]
// debugger
// routes = routes.concat(wallet.routes)
// console.log('routes:', routes)

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
