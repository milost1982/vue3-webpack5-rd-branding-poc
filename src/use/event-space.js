import { ref, computed } from 'vue'

// composition function:
function useEventSpace () {
  const capacity = ref(3)
  const attending = ref(['Tim', 'Bob', 'Joe'])

  const spacesLeft = computed(() => {
    return capacity.value - attending.value.length
  })

  function increment () { capacity.value++ }
  function decrement () {
    if (capacity.value > attending.value.length) {
      capacity.value--
    }
  }

  return { capacity, increment, decrement, attending, spacesLeft }
}

export default useEventSpace
