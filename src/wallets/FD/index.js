function initializeWallet () {
  console.log('Initializing FD wallet')
}

function login () {
  console.log('FD login')
  window.location.href = 'https://account.nj.sportsbook.qa.fndl.dev/login'
}

function register () {
  console.log('FD register')
  window.location.href = 'https://account.nj.sportsbook.qa.fndl.dev/join'
}

function getCustomerContext () {
  console.log('FD login')
}

const name = 'FD Wallet'

export default {
  name,
  initializeWallet,
  login,
  register,
  getCustomerContext
}
