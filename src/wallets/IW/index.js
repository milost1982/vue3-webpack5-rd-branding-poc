// wallet moze da zna za aplikaciju
// aplikacija zna za wallet ali ne zna koji konkretno se koristi
// zato wallet ima interfejs koji aplikacija koristi, i taj interfejs je zajednicki za sve wallete
// a wallet zna sve o aplikaciji tako da moze da koristi/importuje njene servise ili module (npr ruter, vuex store)

// wallet smo apstraktovali iza interfejsa jer app ne zna koji je konkretno wallet, samo zna sta wallet moze

function initializeWallet () {
  console.log('Initializing IW wallet')
}

function login () {
  console.log('IW login')
}

function register () {
  console.log('IW register')
}

function getCustomerContext () {
  console.log('IW login')
}

const name = 'Integrated Wallet'

export default {
  name,
  initializeWallet,
  login,
  register,
  getCustomerContext
}
