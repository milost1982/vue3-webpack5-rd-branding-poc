import Register from './components/Register.vue'

const routes = [
  {
    path: '/account/register',
    name: 'Register',
    component: Register
  }
]

export default routes
