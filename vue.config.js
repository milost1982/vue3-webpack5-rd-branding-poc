// const webpack = require('webpack')
const path = require('path')

// const path = require('path')
const MyFirstWebpackPlugin = require('./build-utils/MyFirstWebpackPlugin')
const BrandComponentResolverPlugin = require('./build-utils/brandComponentResolver')
// const WalletResolverPlugin = require('./build-utils/WalletResolverPlugin')

const { importByBrandEx } = require('./build-utils/importByBrand')
console.log('importByBrandEx: ', importByBrandEx)

const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')
const argv = yargs(hideBin(process.argv)).argv

// console.log('*********** Webpack build, yargs argv:', argv)
if (argv.brand) {
  process.env.VUE_APP_BRAND = argv.brand
  console.log('*********** Webpack build, VUE_APP_BRAND overriden from shell:', argv.brand, process.env.VUE_APP_BRAND)
}
if (argv.wallet) {
  process.env.VUE_APP_WALLET = argv.wallet
  console.log('*********** Webpack build, VUE_APP_WALLET overriden from shell:', argv.brand, process.env.VUE_APP_BRAND)
} else {
  // throw new Error('wallet not specified')
  console.log('wallet not specified, using default IW')
  argv.wallet = 'IW'
}

// function MyConventionResolver (source, target) {
//   this.source = source || 'resolve';
//   this.target = target || 'resolve';
// }

// MyConventionResolver.prototype.apply = function(resolver) {
//   const target = resolver.ensureHook(this.target)
//   resolver.getHook(this.source).tapAsync('MyConventionResolver', function(request, resolveContext, callback) {
//     if (request.request[0] === '#') {
//       const req = request.request.substr(1)
//       const obj = Object.assign({}, request, {
//         // request: req + '/' + path.basename(req) + '.js'
//         request: req
//       })
//       console.log('MyConventionResolver obj:', obj)
//       path.dirname(req)
//       return resolver.doResolve(target, obj, null, resolveContext, callback);
//     }
//     callback()
//   })
// }

module.exports = {
  lintOnSave: process.env.NODE_ENV === 'production' ? 'error' : 'warning',

  configureWebpack: config => {
    // console.log('webpack config:', config)
    // console.log('************* webpack process.env:', process.env)
    console.log('*********** Webpack build, VUE_APP_BRAND:', process.env.VUE_APP_BRAND)
    console.log('*********** Webpack build, mode:', config.mode)
    console.log('*********** Webpack build, process.argv:', process.argv)

    // console.log('*********** Webpack build, TEST123:', config.TEST123)
    // console.log('*********** Webpack build:', process.env)

    if (process.env.NODE_ENV === 'production') {
      // mutate config for production...
    } else {
      // mutate for development...
    }
    return {
      resolve: {
        alias: {
          '#wallet#': path.resolve(__dirname, 'src/wallets/' + argv.wallet)
        },
        plugins: [new BrandComponentResolverPlugin({
          importPrefix: '#brand#',
          brandsDir: 'brands',
          brand: process.env.VUE_APP_BRAND
        })
        // new WalletResolverPlugin({
        //   importPrefix: '#wallet#',
        //   walletsDir: '@/components/wallets',
        //   wallet: process.env.VUE_APP_WALLET
        // })
        ]
      },
      // plugins: [
      //   // new webpack.DefinePlugin({ myWebpackGlobalFunc1: () => console.log('milos tanasijevic') })
      //   new webpack.DefinePlugin({ importByBrandEx })
      // ],
      plugins: [new MyFirstWebpackPlugin()],
      experiments: {
        topLevelAwait: true
      }
    }
  }
}
